lazy val bedrock = (project in file("bedrock")).settings(
  organization := "tf.bug",
  name := "bedrock",
  version := "0.1.0",
  scalaVersion := "2.12.8",
  libraryDependencies ++= Seq(
    "com.github.mpilquist" %%% "simulacrum" % "0.16.0",
    "co.fs2" %%% "fs2-core" % "1.0.4",
  ),
  scalacOptions ++= Seq(
    "-unchecked",
    "-deprecation",
    "-feature",
    "-Ypartial-unification",
    "-language:higherKinds",
  ),
  addCompilerPlugin("org.scalamacros" % "paradise" % "2.1.0" cross CrossVersion.full),
  scalaJSUseMainModuleInitializer := true,
  mainClass := Some("tf.bug.bedrock.test.Test"),
).dependsOn(bedrockfacades).enablePlugins(ScalaJSPlugin)

lazy val bedrockfacades = (project in file("facades")).settings(
  organization := "tf.bug",
  name := "bedrock-facades",
  version := "0.1.0",
  scalaVersion := "2.12.8",
  scalacOptions ++= Seq(
    "-unchecked",
    "-deprecation",
    "-feature",
    "-Ypartial-unification",
    "-language:higherKinds",
  ),
  scalaJSUseMainModuleInitializer := true,
  mainClass := Some("tf.bug.bedrock.facade.test.Test"),
).enablePlugins(ScalaJSPlugin)
