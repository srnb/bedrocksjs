package tf.bug.bedrock.facade.event

import scala.scalajs.js
import scala.scalajs.js.annotation.JSName

@js.native
trait ScriptLoggerConfig extends EventData {

  @JSName("log_errors") var logErrors: Boolean = js.native
  @JSName("log_warnings") var logWarnings: Boolean = js.native
  @JSName("log_information") var logInformation: Boolean = js.native

}
