package tf.bug.bedrock.facade.event

import tf.bug.bedrock.facade.{Entity, Position}

import scala.scalajs.js

@js.native
trait HitResultChanged extends EventData {

  var entity: Entity = js.native

  var position: Position = js.native

}
