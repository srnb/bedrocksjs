package tf.bug.bedrock.facade

import scala.scalajs.js
import scala.scalajs.js.annotation.JSName

@js.native
trait Level extends js.Object {

  @JSName("__type__") val objType: String = js.native

  @JSName("level_id") val levelId: Long = js.native

}
