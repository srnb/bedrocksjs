package tf.bug.bedrock.facade

import scala.scalajs.js
import scala.scalajs.js.annotation.JSName

@js.native
sealed trait TickingArea extends js.Object {

  @JSName("__type__") val objType: String = js.native

}

@js.native
trait EntityTickingArea extends TickingArea {

  @JSName("entity_ticking_area_id") val entityTickingAreaId: Long = js.native

}

@js.native
trait LevelTickingArea extends TickingArea {

  @JSName("level_ticking_area_id") val entityTickingAreaId: Long = js.native

}
