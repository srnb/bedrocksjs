package tf.bug.bedrock.facade.test

import tf.bug.bedrock.facade.event.{DisplayChatEvent, Event, EventData, HitResultChanged, ScriptLoggerConfig}
import tf.bug.bedrock.facade.Globals

object Test {

  def main(args: Array[String]): Unit = {
    val sys = Globals.client.registerSystem(0, 0)
    sys.initialize = () => {
      sys.listenForEvent[HitResultChanged]("minecraft:hit_result_changed", d => onHRC(d))
      val le = sys.createEventData[ScriptLoggerConfig]("minecraft:script_logger_config")
      le.data.logInformation = true
      le.data.logErrors = true
      le.data.logWarnings = true
      sys.broadcastEvent("minecraft:script_logger_config", le)
    }
    def onHRC(value: Event[HitResultChanged]): Unit = {
      val ce = sys.createEventData[DisplayChatEvent]("minecraft:display_chat_event")
      ce.data.message = "\u00a76Gold message\u00a7r Normal message"
      sys.broadcastEvent("minecraft:display_chat_event", ce)
    }
  }

}
