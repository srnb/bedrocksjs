package tf.bug.bedrock.facade

import scala.scalajs.js
import scala.scalajs.js.annotation.JSName

@js.native
trait ItemStack extends js.Object {

  @JSName("__type__") val objType: String = js.native

  @JSName("__identifier__") val objIdentifier: String = js.native

  val item: String = js.native

  val count: String = js.native

}
