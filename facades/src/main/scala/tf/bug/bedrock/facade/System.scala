package tf.bug.bedrock.facade

import tf.bug.bedrock.facade.event.{Event, EventData}

import scala.scalajs.js

@js.native
sealed trait System extends js.Object {

  var initialize: js.Function0[Unit] = js.native

  var update: js.Function0[Unit] = js.native

  var shutdown: js.Function0[Unit] = js.native

  def listenForEvent[T <: EventData](identifier: String, callback: js.Function1[Event[T], Unit]): Boolean = js.native

  def broadcastEvent[T <: EventData](identifier: String, eventData: Event[T]): Boolean = js.native

  def createEventData[T <: EventData](identifier: String): Event[T] = js.native

}

@js.native
sealed trait ClientSystem extends System

@js.native
sealed trait ServerSystem extends System
