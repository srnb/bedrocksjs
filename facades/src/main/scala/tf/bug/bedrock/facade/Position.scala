package tf.bug.bedrock.facade

import scala.scalajs.js

@js.native
class Position(val x: Double, val y: Double, val z: Double) extends js.Object
