package tf.bug.bedrock.facade

import scala.scalajs.js
import scala.scalajs.js.annotation.JSName

@js.native
trait Block extends js.Object {

  @JSName("__type__") val objType: String = js.native

  @JSName("__identifier__") val objIdentifier: String = js.native

  @JSName("ticking_area") val tickingArea: TickingArea = js.native

  @JSName("block_position") val blockPosition: BlockPosition = js.native

}
