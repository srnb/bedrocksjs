package tf.bug.bedrock.facade.event

import scala.scalajs.js

@js.native
sealed trait Event[+T <: EventData] extends js.Object {

  val data: T = js.native

}

@js.native
trait EventData extends js.Object
