package tf.bug.bedrock.facade

import scala.scalajs.js
import scala.scalajs.js.annotation.JSGlobalScope

@js.native
@JSGlobalScope
object Globals extends js.Object {

  val client: ClientSystemFactory = js.native
  val server: ServerSystemFactory = js.native

}
