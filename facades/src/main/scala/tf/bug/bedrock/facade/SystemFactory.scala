package tf.bug.bedrock.facade

import scala.scalajs.js

@js.native
sealed trait SystemFactory[T <: System] extends js.Object {

  def registerSystem(major: Int, minor: Int): T

}

@js.native
sealed trait ClientSystemFactory extends SystemFactory[ClientSystem]

@js.native
sealed trait ServerSystemFactory extends SystemFactory[ServerSystem]
