package tf.bug.bedrock.facade

import scala.scalajs.js
import scala.scalajs.js.annotation.JSName

@js.native
class Entity(@JSName("__type__") val objType: String, @JSName("__identifier__") val objIdentifier: String, val id: Long)
    extends js.Object
