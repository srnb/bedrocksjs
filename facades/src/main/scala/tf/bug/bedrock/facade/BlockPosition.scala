package tf.bug.bedrock.facade

import scala.scalajs.js

@js.native
trait BlockPosition extends js.Object {

  val x: Long = js.native
  val y: Long = js.native
  val z: Long = js.native

}
