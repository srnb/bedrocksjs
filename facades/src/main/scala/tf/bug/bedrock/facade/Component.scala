package tf.bug.bedrock.facade

import scala.scalajs.js
import scala.scalajs.js.annotation.JSName

@js.native
trait Component[T] extends js.Object {

  @JSName("__type__") val objType: String = js.native

  val data: T = js.native

}
