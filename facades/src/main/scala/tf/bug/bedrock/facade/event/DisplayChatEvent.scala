package tf.bug.bedrock.facade.event

import scala.scalajs.js

@js.native
trait DisplayChatEvent extends EventData {

  var message: String = js.native

}
