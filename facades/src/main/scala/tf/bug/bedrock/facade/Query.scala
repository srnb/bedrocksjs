package tf.bug.bedrock.facade

import scala.scalajs.js
import scala.scalajs.js.annotation.JSName

@js.native
trait Query extends js.Object {

  @JSName("__type__") val objType: String = js.native

  @JSName("query_id") val queryId: Long = js.native

}
