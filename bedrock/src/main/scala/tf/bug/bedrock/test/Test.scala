package tf.bug.bedrock.test

import cats._
import cats.implicits._
import cats.effect._
import tf.bug.bedrock.{BedrockClientApp, System}
import tf.bug.bedrock.implicits._
import tf.bug.bedrock.event.{DisplayChatEvent, HitResultChanged, ScriptLoggerConfig}

object Test extends BedrockClientApp {
  override def run(args: List[String]): IO[ExitCode] = {
    program[IO].as(ExitCode.Success)
  }

  def program[F[_]: ConcurrentEffect]: F[Unit] = {
    for {
      c <- System.client[F]
      _ <- c.send(ScriptLoggerConfig(true, true, true))
      _ <- c
        .events[HitResultChanged]
        .evalMap { _ =>
          c.send(DisplayChatEvent("\u00a76Gold message\u00a7r Normal message"))
        }
        .compile
        .drain
    } yield ()
  }
}
