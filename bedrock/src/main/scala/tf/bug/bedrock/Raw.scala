package tf.bug.bedrock

import cats.effect.Sync
import cats.effect.concurrent.Ref
import simulacrum.typeclass
import tf.bug.bedrock.facade.event.EventData

@typeclass trait Raw[T] {

  type Js

  def fill[F[_]: Sync](sys: System[F], ev: T): F[Ref[F, Js]]

  def from[F[_]: Sync](sys: System[F], objRef: Ref[F, Js]): F[T]

}

object Raw {

  type Aux[A, J] = Raw[A] { type Js = J }

}
