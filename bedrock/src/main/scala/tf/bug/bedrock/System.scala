package tf.bug.bedrock

import cats._
import cats.implicits._
import cats.effect._
import cats.effect.concurrent.Ref
import fs2._
import fs2.concurrent.Queue
import tf.bug.bedrock.event.Event

object System {

  def client[F[_]: ConcurrentEffect]: F[Client[F]] =
    for {
      i <- Sync[F].delay(facade.Globals.client.registerSystem(0, 0))
      tup <- hook(i)
      (initialize, update, shutdown) = tup
    } yield Client[F](i, initialize, update, shutdown)

  def server[F[_]: ConcurrentEffect]: F[Server[F]] =
    for {
      i <- Sync[F].delay(facade.Globals.server.registerSystem(0, 0))
      tup <- hook(i)
      (initialize, update, shutdown) = tup
    } yield Server[F](i, initialize, update, shutdown)

  def hook[F[_]: ConcurrentEffect](ii: facade.System): F[(F[Unit], Stream[F, Unit], F[Unit])] = {
    for {
      i <- Applicative[F].pure(ii)
      initialize = Async[F].async[Unit] { cb =>
        val previousInit = Option(i.initialize)
        i.initialize = () => {
          previousInit.foreach(_.apply())
          cb(Right(()))
        }
      }
      update = for {
        q <- Stream.eval(Queue.unbounded[F, Unit])
        _ <- Stream.eval {
          Sync[F].delay {
            val previousUpdate = Option(i.update)
            i.update = () => {
              previousUpdate.foreach(_.apply())
              Effect[F].runAsync(q.enqueue1(()))(_ => IO.unit).unsafeRunSync()
            }
          }
        }
        d <- q.dequeue
      } yield d
      shutdown = Async[F].async[Unit] { cb =>
        val previousShutdown = Option(i.shutdown)
        i.shutdown = () => {
          previousShutdown.foreach(_.apply())
          cb(Right(()))
        }
      }
    } yield (initialize, update, shutdown)
  }

}

sealed trait System[F[_]] {

  val i: facade.System

  val initialize: F[Unit]
  val update: Stream[F, Unit]
  val shutdown: F[Unit]

  def events[T](implicit cce: ConcurrentEffect[F], ev: Event[T]): Stream[F, T] = {
    for {
      q <- Stream.eval(Queue.unbounded[F, T])
      _ <- Stream.eval {
        cce.delay {
          i.listenForEvent[ev.Jt](ev.name.toString, e => {
            val f = for {
              r <- Ref.of[F, ev.Js](e)
              t <- ev.from(this, r)
            } yield t
            cce.runAsync(f.flatMap(q.enqueue1))(_ => IO.unit).unsafeRunSync()
          })
        }
      }
      d <- q.dequeue
    } yield d
  }

  def send[T](e: T)(implicit sync: Sync[F], ev: Event[T]): F[Unit] = {
    for {
      r <- ev.fill[F](this, e)
      e <- r.get
      s <- sync.delay({ i.broadcastEvent[ev.Jt](ev.name.toString, e); () })
    } yield s
  }

}

case class Client[F[_]](i: facade.ClientSystem, initialize: F[Unit], update: Stream[F, Unit], shutdown: F[Unit])
    extends System[F]

case class Server[F[_]](i: facade.ServerSystem, initialize: F[Unit], update: Stream[F, Unit], shutdown: F[Unit])
    extends System[F]
