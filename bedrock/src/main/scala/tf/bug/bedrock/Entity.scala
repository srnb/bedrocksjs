package tf.bug.bedrock

case class Entity(objType: String, objId: String, id: Long)
