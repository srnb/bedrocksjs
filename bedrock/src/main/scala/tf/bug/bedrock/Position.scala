package tf.bug.bedrock

case class Position(x: Double, y: Double, z: Double)
