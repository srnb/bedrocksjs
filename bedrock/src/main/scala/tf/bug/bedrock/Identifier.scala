package tf.bug.bedrock

case class Identifier(namespace: String, id: String) {

  override def toString: String = s"$namespace:$id"

}
