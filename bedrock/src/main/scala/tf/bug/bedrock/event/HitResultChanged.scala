package tf.bug.bedrock.event

import tf.bug.bedrock.{Entity, Position}

case class HitResultChanged(entity: Entity, position: Position)
