package tf.bug.bedrock.event

case class ScriptLoggerConfig(logErrors: Boolean, logWarnings: Boolean, logInfo: Boolean)
