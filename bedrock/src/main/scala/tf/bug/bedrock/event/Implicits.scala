package tf.bug.bedrock.event

import cats._
import cats.implicits._
import cats.effect._
import cats.effect.concurrent.Ref
import tf.bug.bedrock
import tf.bug.bedrock.implicits._
import tf.bug.bedrock.{Entity, Identifier, Position, Raw}
import tf.bug.bedrock.facade
import tf.bug.bedrock.facade.event

trait Implicits {

  implicit val hitResultChangedEvent: Event[HitResultChanged] = new Event[HitResultChanged] {
    override def name: Identifier = Identifier("minecraft", "hit_result_changed")

    override type Jt = event.HitResultChanged

    override def fill[F[_]: Sync](sys: bedrock.System[F], ev: HitResultChanged, objRef: Ref[F, Js]): F[Unit] = {
      val entRaw: Raw.Aux[Entity, facade.Entity] = implicitly[Raw.Aux[Entity, facade.Entity]]
      val posRaw: Raw.Aux[Position, facade.Position] = implicitly[Raw.Aux[Position, facade.Position]]
      for {
        entR <- entRaw.fill(sys, ev.entity)
        ent <- entR.get
        posR <- posRaw.fill(sys, ev.position)
        pos <- posR.get
        set <- objRef.update { e =>
          e.data.entity = ent
          e.data.position = pos
          e
        }
      } yield set
    }

    override def from[F[_]: Sync](sys: bedrock.System[F], objRef: Ref[F, Js]): F[HitResultChanged] = {
      val entRaw: Raw.Aux[Entity, facade.Entity] = implicitly[Raw.Aux[Entity, facade.Entity]]
      val posRaw: Raw.Aux[Position, facade.Position] = implicitly[Raw.Aux[Position, facade.Position]]
      for {
        js <- objRef.get
        entR <- Ref.of(js.data.entity)
        ent <- entRaw.from(sys, entR)
        posR <- Ref.of(js.data.position)
        pos <- posRaw.from(sys, posR)
      } yield HitResultChanged(ent, pos)
    }
  }

  implicit val displayChatEvent: Event[DisplayChatEvent] = new Event[DisplayChatEvent] {
    override type Jt = event.DisplayChatEvent

    override def name: Identifier = Identifier("minecraft", "display_chat_event")

    override def fill[F[_]: Sync](sys: bedrock.System[F], ev: DisplayChatEvent, o: Ref[F, Js]): F[Unit] = {
      o.update { e =>
        e.data.message = ev.message
        e
      }
    }

    override def from[F[_]: Sync](sys: bedrock.System[F], objRef: Ref[F, Js]): F[DisplayChatEvent] = {
      for {
        js <- objRef.get
      } yield DisplayChatEvent(js.data.message)
    }
  }

  implicit val scriptLoggerConfigEvent: Event[ScriptLoggerConfig] = new Event[ScriptLoggerConfig] {
    override type Jt = event.ScriptLoggerConfig

    override def name: Identifier = Identifier("minecraft", "script_logger_config")

    override def fill[F[_]: Sync](sys: bedrock.System[F], ev: ScriptLoggerConfig, o: Ref[F, Js]): F[Unit] = {
      o.update { e =>
        e.data.logErrors = ev.logErrors
        e.data.logWarnings = ev.logWarnings
        e.data.logInformation = ev.logInfo
        e
      }
    }

    override def from[F[_]: Sync](sys: bedrock.System[F], objRef: Ref[F, Js]): F[ScriptLoggerConfig] = {
      for {
        js <- objRef.get
      } yield ScriptLoggerConfig(js.data.logErrors, js.data.logWarnings, js.data.logInformation)
    }
  }

}
