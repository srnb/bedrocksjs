package tf.bug.bedrock.event

import cats._
import cats.implicits._
import cats.effect._
import cats.effect.concurrent.Ref
import simulacrum.typeclass
import tf.bug.bedrock.facade.event.EventData
import tf.bug.bedrock.{facade, Identifier, Raw, System}

@typeclass trait Event[T] extends Raw[T] {

  final override type Js = facade.event.Event[Jt]

  type Jt <: EventData

  def name: Identifier

  def fill[F[_]: Sync](sys: System[F], ev: T, o: Ref[F, Js]): F[Unit]

  final override def fill[F[_]: Sync](sys: System[F], ev: T): F[Ref[F, Js]] = {
    for {
      r <- Ref.of[F, Js](sys.i.createEventData[Jt](name.toString))
      _ <- fill(sys, ev, r)
    } yield r
  }

}
