package tf.bug.bedrock.event

case class DisplayChatEvent(message: String)
