package tf.bug.bedrock

import cats._
import cats.implicits._
import cats.effect._
import cats.effect.concurrent.Ref

object implicits extends event.Implicits {

  implicit val entityRaw: Raw.Aux[Entity, facade.Entity] = new Raw[Entity] {

    override type Js = facade.Entity

    override def fill[F[_]: Sync](sys: System[F], ev: Entity): F[Ref[F, facade.Entity]] = {
      Ref.of(new facade.Entity(ev.objType, ev.objId, ev.id))
    }

    override def from[F[_]: Sync](sys: System[F], objRef: Ref[F, facade.Entity]): F[Entity] = {
      for {
        ent <- objRef.get
      } yield Entity(ent.objIdentifier, ent.objType, ent.id)
    }

  }

  implicit val positionRaw: Raw.Aux[Position, facade.Position] = new Raw[Position] {

    override type Js = facade.Position

    override def fill[F[_]: Sync](sys: System[F], ev: Position): F[Ref[F, facade.Position]] = {
      Ref.of(new facade.Position(ev.x, ev.y, ev.z))
    }

    override def from[F[_]: Sync](sys: System[F], objRef: Ref[F, facade.Position]): F[Position] = {
      for {
        pos <- objRef.get
      } yield Position(pos.x, pos.y, pos.z)
    }

  }

}
