package tf.bug.bedrock

import cats.effect.IO.Async
import cats.effect.{Clock, ContextShift, IO, IOApp, Timer}

import scala.collection.mutable.ArrayBuffer
import scala.concurrent.ExecutionContext
import scala.concurrent.duration.{FiniteDuration, TimeUnit}

trait BedrockClientApp extends IOApp {

  private val system = facade.Globals.client.registerSystem(0, 0)
  private var tickTime = 0l
  private val handlers = ArrayBuffer[() => Unit](() => tickTime = tickTime + 1l)

  private def updateHandlers(): Unit = {
    system.update = () => handlers.foreach(_.apply())
  }

  private def addHandler(h: () => Unit): Unit = {
    handlers += h
    updateHandlers()
  }

  private def removeHandler(h: () => Unit): Unit = {
    handlers -= h
    updateHandlers()
  }

  private def afterTicks(ticks: Long, cb: Either[Throwable, Unit] => Unit): Unit = {
    var i = ticks
    lazy val h: () => Unit = () => {
      if (i < 0) {
        cb(Right(()))
        removeHandler(h)
      }
      i = i - 1
    }
    addHandler(h)
  }

  override implicit protected def timer: Timer[IO] = new Timer[IO] {
    override def clock: Clock[IO] = new Clock[IO] {
      override def realTime(unit: TimeUnit): IO[Long] = IO(java.lang.System.currentTimeMillis())

      override def monotonic(unit: TimeUnit): IO[Long] = IO(tickTime)
    }

    override def sleep(duration: FiniteDuration): IO[Unit] = {
      val ms = duration.toMillis
      val ticks = ms / 50.toLong
      IO.async(afterTicks(ticks, _))
    }
  }

  override implicit protected def contextShift: ContextShift[IO] = {
    new ContextShift[IO] {
      override def shift: IO[Unit] = IO.async(afterTicks(0, _))

      override def evalOn[A](ec: ExecutionContext)(fa: IO[A]): IO[A] = fa
    }
  }

}
